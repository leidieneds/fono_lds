from json import loads
from urllib.request import urlopen
from pickle import dump, load
URL = "http://activufrj.nce.ufrj.br/api/getlist"
URLREG = "http://activufrj.nce.ufrj.br/api/getsession?id="


class Activ:
    def __init__(self):  # underline em python quer dizer qu eé um nome reservado, um nome com significado predefinido
        self.jogadores = None

    def one_player(self, play_url='90235c60e284c8ec5b8fc4107300f398'):

        dataset = urlopen(URL)  # por favor abra o pacote
        pyset = loads(dataset.read())  # vai transformar os strings do json
        registros = pyset['applist']  # registro principal que esta com o Mauricio
        registros2018 = [numreg for data, numreg in registros if data and "2018" in data]
        print(len(registros2018), registros2018)
        urlreg1 = '8d168e9dfafc345d7a4c1dfe15003440'
        self.players = [self.load_player(player_id) for player_id in registros2018[:]]
        #self.load_player(urlreg1)

    def load_player(self, player):
        aluno1 = urlopen(URLREG + player)
        pyset = loads(aluno1.read())
        #print(len(pyset), pyset["session"])
        jogador = Jogador(**pyset["session"])
        jogador.load_games(pyset["games"])
        return jogador
        #print ("jogador", jogador, jogador.sexo2, jogador.idade1, jogador.idade2,
               #jogador.ano1, jogador.ano2, jogador.escola, jogador.sexo1, jogador.starttime,
               #jogador.tipoescola, jogador.endtime)

    def get_jogadores(self):
        def load_registros():
            dataset = urlopen(URL)  # por favor abra o pacote
            pyset = loads(dataset.read())  # vai transformar os strings do json
            registros = pyset['applist']  # registro principal que esta com o Mauricio
            registros2018 = [numreg for data, numreg in registros if data and "2018" in data]
            print(len(registros2018), registros2018)
            return registros2018

        def get_jogador(urlreg):
            urlreg1 = URLREG + urlreg
            aluno1 = urlopen(urlreg1)
            pyset = loads(aluno1.read())
            print(len(pyset), pyset["session"])
            jogador = Jogador(games=pyset["games"], **pyset["session"])
            return jogador
        pic = open
        jogador.plk", "wb")
        self.jogadores = [get_jogador(jog_url) for jog_url in load_registros()]
        dump(self, pic)

    @staticmethod
    def load_jogadores():
        pic = open("jogador.plk", "rb")
        return load(pic)

    def get_gamers(self, name="tol"):
        return {ind: [game for game in player.games if game.has_game(name) ]for ind, player in enumerate(self.jogadores)}


class Jogada:
    def __init__(self, xpos, house, ypos, player, state,
                 score, result, time, marker, categoria="",
                 cor="", acertos=0, outros="", forma="",
                 numero=0, carta_resposta=""):
        print("      Jogada", xpos, house,ypos, player,
              state, score, result, time, marker, categoria,
              cor, acertos, outros, forma, numero, carta_resposta)
        self.xpos, self.house, self.ypos, self.player, self.marker = xpos, house, ypos, player, marker
        self.state, self.score, self.result, self.time = state, score, result, time
        self.categoria, self.cor, self.acertos, self.outros, self.forma = categoria, cor, acertos, outros, forma
        self.numero, self.carta_resposta = numero, carta_resposta


class Trial:
    def __init__(self, trial, criteria, markers, houses, headings, time, level):
        print("  Trial", criteria, markers, houses,
              headings, time, level)
        print("  Trial - jogada", list(trial[0][0].keys() if trial else []))
        self.trial = [Jogada(**params) for params in trial[0]] if trial else []
        self.criteria, self.markers, self.houses = criteria, markers, houses
        self.headings, self.time, self.level = headings, time, level

class Game:
    def __init__(self, time, maxlevel, goal, name):
        print("Game", time, maxlevel, name, list(goal[0].keys()))
        self.time, self.maxlevel, self.name = time, maxlevel, name
        self.goal = [Trial(**params) for params in goal]


class Jogador:
    def __init__(self, games, starttime, sexo2, idade1, idade2, ano1, ano2, escola, sexo1,
                 tipoescola, endtime):
        self.session = 0
        self.sexo2 = sexo2
        self.starttime = starttime
        self.idade1 = idade1
        self.idade2 = idade2
        self.ano1 = ano1
        self.ano2 = ano2
        self.escola = escola
        self.sexo1 = sexo1
        self.tipoescola = tipoescola
        self.endtime = endtime
        print("Jogador", starttime, idade1, idade2, ano1, ano2, escola, sexo1, sexo2, tipoescola, endtime)
        self.game = [Game(**params) for params in games]


def load_games(self, games):
        self.games = [Game(**params) for params in games]



if __name__ == '__main__':
   Activ().get_jogadores()